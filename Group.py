class Group:
    def __init__(self, name):
        self.name = name
        self.users = []

    def addUser(self, User):
        self.users.append(User)

    def deleteUser(self, User):
        self.users.remove(User)
    
    def __str__(self):
        usersNames = []
        for user in self.users:
            usersNames.append(user.username)
        return f'Grupo: {self.name}, Usuarios:{usersNames}'


class Groups:
    def __init__(self):
        self.groups = []

    #Crea un grupo
    def createGroup(self):
        print('Ingresa el nombre del grupo')
        name = input()
        if self.validateGroup(name):
            newGroup = Group(name)
            self.groups.append(newGroup)    

    #Valida la creacion del grpo
    def validateGroup(self, name):
        if name.strip() == '':
            print('El nombre no puede estar vacio')
            return False
        
        if(self.findByName(name) != -1):
            return False

        return True

    #devuelve un grpo con el nombre buscado, sino encuentra devuelve -1
    def findByName(self, name):
        result = list( filter(lambda gruop: gruop.name.lower() == name.lower(), self.groups) )
        if len(result) == 0:
            return -1
        return result[0]

        
    #Lista los grupos existentes
    def listGruops(self):
        for group in self.groups:
            print(group)